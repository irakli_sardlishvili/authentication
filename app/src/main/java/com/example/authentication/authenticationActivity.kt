package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_authentication.*

class authenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        init()
    }
    private fun init(){
        sign_up_button.setOnClickListener(){
            val intent = Intent(this, signUpActivity::class.java)
            startActivity(intent)
        }
        sign_in_button.setOnClickListener(){
            val intent = Intent(this, signInActivity::class.java)
            startActivity(intent)
        }
    }
}