package com.example.authentication

import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class signUpActivity : AppCompatActivity() {

    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        sign_up_button_1.setOnClickListener(){
            sign_up()
        }
    }

    private fun sign_up(){
        val email = email_input_su.text.toString()
        val password = password_input_su.text.toString()
        val repeat_password = repeat_password_input.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty() && repeat_password.isNotEmpty()) {
            if ("@gmail.com" in email){
                if (password == repeat_password) {
                    progressbar.visibility = View.VISIBLE
                    sign_up_button_1.isClickable = false
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            progressbar.visibility = View.INVISIBLE
                            sign_up_button_1.isClickable = true
                            if (task.isSuccessful) {
                                d("sign up", "createUserWithEmail:success")
                                val user = auth.currentUser
                                Toast.makeText(this, "Sign up is successful", Toast.LENGTH_SHORT).show()
                            } else {
                                d("sign up", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(baseContext, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show()

                            }
                        }
                }else {
                    Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this, "Wrong email address", Toast.LENGTH_SHORT).show()
            }
        }else {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }
}