package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_in.*

class signInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        init()
    }

    private fun init(){
        auth = Firebase.auth
        sign_in_button_1.setOnClickListener(){
            sign_in()
        }
    }

    private fun sign_in(){
        val email = email_input_si.text.toString()
        val password = password_input_si.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()){
            progressbar1.visibility = View.VISIBLE
            sign_in_button_1.isClickable = false
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    progressbar1.visibility = View.INVISIBLE
                    sign_in_button_1.isClickable = true
                    if (task.isSuccessful) {
                        Log.d("sign in", "signInWithEmail:success")
                        val user = auth.currentUser
                        Toast.makeText(this, "Authentication is successful", Toast.LENGTH_SHORT).show()
                    } else {
                        Log.d("sign in", "signInWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()

                    }

                }
        }else{
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }
}